package com.hk.practice;

import android.util.Log;

import com.hk.practice.user.UserItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;

public class QueryUtils {
    private static final String LOG_TAG = QueryUtils.class.getName();
    private final static String URL = "https://jsonplaceholder.typicode.com/";


    /**
     * This class for fetching data from {@value URL} and saving it into the list
     *
     * Constructor is private because no one should ever create a {@link QueryUtils} object.
     * This class is only meant to hold static variables and methods, which can be accessed
     * directly from the class name QueryUtils (and an object instance of QueryUtils is not needed).
     */

    private QueryUtils() {
    }

    public static ArrayList<UserItem> fetchUserInfo() {
        URL url = createUrl();

        String jsonResponse = null;

        try {
            jsonResponse = httpsRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return extractEarthquakes(jsonResponse);
    }

    private static URL createUrl() {
        String urlString = URL + "users";

        URL url = null;

        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Problem while creating url.", e);
        }
        return url;
    }


    private static String httpsRequest(URL url) throws IOException{
        String jsonObject = null;

        if (url == null) {
            return null;
        }

        HttpsURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(15000);
            urlConnection.setReadTimeout(10000);
            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();

            if (responseCode == 200) {
                inputStream = urlConnection.getInputStream();
                jsonObject = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + responseCode);
            }
        } catch (IOException e) {
            Log.e(LOG_TAG,"Error while retrieving jsonObject", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonObject;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        if (inputStream != null) {
            InputStreamReader streamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(streamReader);
            String line = reader.readLine();

            while (line != null) {
                stringBuilder.append(line);
                line = reader.readLine();
            }
        }
        return stringBuilder.toString();
    }


    /**
     * Return a list of {@link UserItem} objects that has been built up from
     * parsing a JSON response.
     */
    private static ArrayList<UserItem> extractEarthquakes(String jsonResponse) {

        // Create an empty ArrayList that we can start adding users info to
        ArrayList<UserItem> users = new ArrayList<>();

        // Tries to parse the SAMPLE_JSON_RESPONSE. If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        try {
            JSONArray usersJsonArray = new JSONArray(jsonResponse);

            for (int i = 0; i < usersJsonArray.length(); i++) {
                JSONObject currantUser = usersJsonArray.getJSONObject(i);
                int userID = currantUser.getInt("id");
                String username = currantUser.getString("username");
                String email = currantUser.getString("email");

                users.add(new UserItem(userID, username, email));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing the earthquake JSON results", e);
        }

        return users;
    }
}
