package com.hk.practice.user;


/**
 * User item representing a piece of content.
 */
public class UserItem {
    public final int id;
    public final String username;
    public final String email;

    public UserItem(int id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
    }

    @Override
    public String toString() {
        return username;
    }
}
