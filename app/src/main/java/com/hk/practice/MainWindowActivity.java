package com.hk.practice;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.Toast;


public class MainWindowActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private ActionBarDrawerToggle mToggle;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_window);

        initViews();
        attachFragment(savedInstanceState);
        createDrawer();
        drawerItemClick();
    }

    private void initViews() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView = findViewById(R.id.nav_view);
        mToolbar = findViewById(R.id.toolbar);
    }

    /**
     * Attach {@link UserListFragment} to the activity
     */
    private void attachFragment(Bundle savedInstanceState) {
        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            UserListFragment userListFragment = new UserListFragment();

            getFragmentManager().beginTransaction().replace(R.id.fragment_container, userListFragment).commit();
        }
    }

    /**
     * Makes a toolbar as an actionBar
     * Creates navigation drawer with four items
     * (User, Page 1, Page 2 and Log out).
     */
    private void createDrawer() {
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open_toggle, R.string.close_toggle);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();


        setSupportActionBar(mToolbar);


        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        actionBar.setTitle("");
    }


    /**
     * Handles navigation drawer item clicks.
     * Than closes drawer.
     */
    private void drawerItemClick() {
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case (R.id.nav_users):
                        Toast.makeText(MainWindowActivity.this, "Hello", Toast.LENGTH_SHORT).show();
                        break;
                    case (R.id.nav_page_1):

                        break;
                    case (R.id.nav_page_2):

                        break;
                    default:

                        break;
                }

                item.setChecked(true);
                mDrawerLayout.closeDrawers();

                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)) {
            mDrawerLayout.openDrawer(Gravity.START);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


