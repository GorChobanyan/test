package com.hk.practice;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hk.practice.UserListFragment.OnListFragmentInteractionListener;
import com.hk.practice.user.UserItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link UserItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final List<UserItem> mValues;
    private final OnListFragmentInteractionListener mListener;

    protected RecyclerViewAdapter(List<UserItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.username.setText(mValues.get(position).username);
        holder.userEmail.setText(mValues.get(position).email);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onUserListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final View mView;
        public final TextView username;
        private final TextView userEmail;
        public UserItem mItem;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            username = view.findViewById(R.id.username);
            userEmail = view.findViewById(R.id.user_email);
        }
    }
}
