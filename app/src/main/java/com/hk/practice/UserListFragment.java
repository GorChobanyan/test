package com.hk.practice;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hk.practice.user.UserItem;

import java.util.ArrayList;

/**
 * A fragment represents a list of users.
 * Users contain 2 textViews for username and email
 * When item is clicked the screen is directed to an album fragment
 */
public class UserListFragment extends Fragment {

    private RecyclerView mRecyclerView;

    public UserListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;

            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            (new UserInfoAsyncTask()).execute();

        }
        return view;
    }

    /**
     * AsyncTask for updating recyclerView with user items by downloading .
     */
    private class UserInfoAsyncTask extends AsyncTask<Void, Void, ArrayList<UserItem>> {

        @Override
        protected ArrayList<UserItem> doInBackground(Void... voids) {
            return QueryUtils.fetchUserInfo();
        }

        @Override
        protected void onPostExecute(ArrayList<UserItem> userItems) {
            super.onPostExecute(userItems);
            mRecyclerView.setAdapter(new RecyclerViewAdapter(userItems, new OnListFragmentInteractionListener() {
                @Override
                public void onUserListFragmentInteraction(UserItem item) {
                    AlbumFragment albumFragment = new AlbumFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();

                    transaction.replace(R.id.fragment_container, albumFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }));
        }
    }

    public interface OnListFragmentInteractionListener {
        void onUserListFragmentInteraction(UserItem item);
    }
}
