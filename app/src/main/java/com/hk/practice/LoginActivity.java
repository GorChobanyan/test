package com.hk.practice;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends FragmentActivity {
    private Button signInButton;
    private EditText emailText;
    private EditText passwordText;

    //Minimum password length
    private final int MIN_PASSWORD_LENGTH = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();

        //Checks if devise is connected to the internet. If it is not show no internet message else start MainWindowActivity
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isConnected()) {
                    Toast.makeText(LoginActivity.this, R.string.no_internet_message, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isValidInput()) {
                    Intent startMainWindowActivity = new Intent(LoginActivity.this, MainWindowActivity.class);
                    startActivity(startMainWindowActivity);
                }
            }
        });
    }

    private void initViews() {
        signInButton = findViewById(R.id.sign_in_button);
        emailText = findViewById(R.id.username_edit_text);
        passwordText = findViewById(R.id.password_edit_text);
    }

    /**
     * Checks if user enters a valid username
     * and password with the length larger or equal than 6
     * @return true if input is valid, false if it doesn't satisfy condition in the description
     */
    public boolean isValidInput() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError(getString(R.string.not_valid_email));
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < MIN_PASSWORD_LENGTH) {
            passwordText.setError(String.format(getString(R.string.not_valid_password), MIN_PASSWORD_LENGTH));
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }


    /**
     * Checks if devise is connected to the internet.
     * @return true if it is connected, false if it is not connected
     */
    private boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}